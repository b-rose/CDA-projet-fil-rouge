# CDA Projet Fil Rouge

Ce projet contient un POC de l'application recycla-score ayant pour but d'informer les utilisateurs d'à quel point est recyclable un objet donné dans le même principe que le nutriscore.

Pour démarrer l'application veuillez suivre les instructions suivantes :
Cloner le projet en local
Lancer mysql -u admin_user -p fil_rouge < dump.sql
Lancer un server tomcat sur la classe filerouge_back/src/main/java/com/cda/filerouge_back/FilRougeApplication.java
Lancer ng serve à partir du dossier FileRouge_front
L'application est alors disponible à l'URL http://localhost:4200

La documentation, du cahier des charges à la documentation technique d'un composant métier est dans le fichier Documentation_FilRouge_CDA_B_ROSE.
